<?php get_header(); ?>

	<section class="frase mobile">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<p>Para você ficar por dentro de diversos assuntos da reprodução assistida e da saúde integral feminina.</p>
				</div>
			</div>
		</div>
	</section>

	<?php while(have_posts()): ?>
	<?php the_post(); ?>

	<section class="blogposts">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
		<div class="grad"></div>
		<div class="container">
			<div class="row itens justify-content-center">
				<?php $paged = (get_query_var('paged'))?get_query_var('paged'):1; ?>
				<?php $query = new WP_Query(array( 
					'post_type' => 'post',
					'posts_per_page' => 6,
					'order' => 'DESC',
					'paged' => $paged 
				)); ?>
				<?php if($query->have_posts()): ?>
					<?php $postsBlog = array(); ?>
					<?php while($query->have_posts()): $query->the_post(); ?>
						<?php $index = $query->current_post; ?>

						<?php $postsBlog[$index]['index'] = $index; ?>
						<?php $postsBlog[$index]['title'] = get_the_title(); ?>
						<?php $postsBlog[$index]['permalink'] = get_the_permalink(); ?>
						<?php $postsBlog[$index]['excerpt'] = get_the_excerpt(); ?>
						<?php $postsBlog[$index]['image'] = get_the_post_thumbnail_url(); ?>

					<?php endwhile; ?>
					<div class="col-12 col-lg-4 align-self-start desktop">
						<?php foreach ($postsBlog as $index => $item): ?>
						<?php if(($index % 2) == 0): ?>
						<div class="item<?php echo ' item'.$index . (($index == 1)?' margin':''); ?>">
							<a href="<?php echo $item['permalink']; ?>">
								<div class="img" style="background-image: url(<?php echo $item['image']; ?>);"></div>
								<h2><?php echo $item['title']; ?></h2>
								<p><?php echo $item['excerpt']; ?>.</p>
								<button>Saiba mais</button>
							</a>
						</div>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
					<div class="col-12 col-lg-4 align-self-start desktop">
						<?php foreach ($postsBlog as $index => $item): ?>
						<?php if(($index % 2) != 0): ?>
						<div class="item<?php echo ' item'.$index . (($index == 1)?' margin':''); ?>">
							<a href="<?php echo $item['permalink']; ?>">
								<div class="img" style="background-image: url(<?php echo $item['image']; ?>);"></div>
								<h2><?php echo $item['title']; ?></h2>
								<p><?php echo $item['excerpt']; ?>.</p>
								<button>Saiba mais</button>
							</a>
						</div>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
					<div class="col-10 col-lg-4 align-self-center mobile">
						<?php foreach ($postsBlog as $index => $item): ?>
						<div class="item<?php echo ' item'.$index . (($index == 1)?' margin':''); ?>">
							<a href="<?php echo $item['permalink']; ?>">
								<div class="img" style="background-image: url(<?php echo $item['image']; ?>);"></div>
								<h2><?php echo $item['title']; ?></h2>
								<p><?php echo $item['excerpt']; ?>.</p>
								<button>Saiba mais</button>
							</a>
						</div>
						<?php endforeach; ?>
					</div>
					<div class="paginacao col-12 col-lg-8">
						<?php pagination($query); ?>
					</div>
				<?php else: ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<?php endwhile; ?>

<?php get_footer();