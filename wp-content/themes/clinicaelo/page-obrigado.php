<?php get_header('obrigado'); ?>


	<section class="obrigado">
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-12 col-lg-8 align-self-center">
					<p>Com esse e-book em suas mãos, esperamos te auxiliar no esclarecimento de dúvidas sobre diversos assuntos da reprodução assistida.</p>

					<p>Sempre teremos novos e-books para te manter informada e ajudá-la da melhor forma possível apresentando <strong>as muitas possibilidades que a medicina reprodutiva disponibilza atualmente para a realização do sonho da maternidade</strong>.</p>

					<p class="margin50">Surgindo novas questões, deixe sua dúvida por meio do botão do whatsapp ao lado.</p>

					<button><img src="<?php echo get_template_directory_uri(); ?>/assets/img/data.png" /> Clique aqui e pré-agende sua consulta.</button>

					<h2>Alameda dos Maracatins  •  Nº 426  •  Conjunto 804<br/> Moema, São Paulo/SP  •  CEP 04089-010</h2>

					<h3>Tel.: (11) 3805-9754  •  (11) 99999-9999M/
				</div>
			</div>
		</div>
	</section>

<?php get_footer('obrigado');