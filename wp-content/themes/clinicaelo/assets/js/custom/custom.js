
function menu() {
     if ($(window).scrollTop() > 30)  {
          $('.floatMenu').addClass('open');
          $('#logo')
               .addClass('col-md-6 order-md-1 order-lg-2');
          $('#search')
               .removeClass('col-lg-6')
               .addClass('col-lg-5 order-lg-3');
          $('#site-navigation')
               .removeClass('col-lg-12')
               .addClass('col-md-6 col-lg-1 order-md-2 order-lg-1');

          $('.scrollTop').addClass('show');
     }  else {
          $('.floatMenu').removeClass('open');

          $('#logo')
               .removeClass('col-md-6 order-md-1 order-lg-2');
          $('#search')
               .removeClass('col-lg-5 order-lg-3')
               .addClass('col-lg-6');
          $('#site-navigation')
               .removeClass('col-md-6 col-lg-1 order-md-2 order-lg-1')
               .addClass('col-lg-12');

          $('.scrollTop').removeClass('show');
     }
}


function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop() + 104;
    var docViewBottom = docViewTop + ($(window).height());

    var elemTop = $(elem).offset().top + 50;
    var elemBottom = elemTop + ($(elem).height());

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function num(num) {
   return num % 1 === 0;
}

function wasScrolled(elm) {
     if ((isScrolledIntoView(elm)) && (!elm.hasClass('scrolled'))) {
          
          elm.addClass('scrolled');

          var can = elm.children('canvas');
          var val = elm.children('.valor').text();

          if (can.length) {
               var can = can.attr('id');

               var graph = new Chart(document.getElementById(can).getContext('2d'), {
                    type: 'pie',
                    data: {
                         datasets: [{
                              data: [val, (100 - val)],
                              backgroundColor: [
                                   '#671B51',
                                   '#BEA07B'
                              ],
                              borderWidth: 0
                         }],
                         labels: [
                              '',
                              ''
                         ]
                    },
                    options: {
                         animation: {
                              duration: 4000
                         },
                         legend: {
                              display: false
                         },
                         borderWidth: 0,
                         layout: {
                              padding: {
                                   left: 0,
                                   right: 0,
                                   top: 0,
                                   bottom: 0
                              }
                         },        
                         responsive: false,
                         maintainAspectRatio: true
                    }
               });
          } else {
               elm.children('.valor').each(function () {
                    $(this).prop('Counter',0).animate({
                         Counter: $(this).text()
                    }, {
                         duration: 4000,
                         easing: 'swing',
                         step: function (now) {
                              if (num(val)) {
                                   $(this).text(Math.round(now));
                              } else {
                                   $(this).text(Math.round(now * 10) / 10);
                              }
                         }
                    });
               });               
          }
     }
}

function scrolling() {
     if ($('#Abas li').length) {
          for (var i = 2; i <= 22; i++) {
               wasScrolled($(".item.item" + i));
          }
     }
}

$(document).ready(function(){
     menu();

     $('.scrollTop').on('click', function(){
          $("html, body").animate({ scrollTop: "0" });
     });
     
     $('div[data-validate=celular] input').mask('(00) 00000-0000');

     $('.mauticform-selectbox').on('change', function(){
          if ($(this).val()) {
               $(this).addClass('valor');
          } else {
               $(this).removeClass('valor');
          }
     });

     $('#Abas li').on('click', function(){
          $('html, body').animate({
               scrollTop: $("#Resultado").offset().top
          });
          $('#Abas li').removeClass('ativo');
          $(this).addClass('ativo');
     });

     $('#menu-icone').on('click', function(){
          $(this).toggleClass('open');
          if ($('#site-navigation .menu-container').hasClass('show')) {
               $('#site-navigation .menu-container').removeClass('show');
               setTimeout(function() {
                    $('#site-navigation .menu-container').removeClass('open');
               }, 400);
          } else {
               $('#site-navigation .menu-container').addClass('open');
               setTimeout(function() {
                    $('#site-navigation .menu-container').addClass('show');
               }, 1);
          }
     });
     $('#lupa-icone').on('click', function(){
          $('#searchform input').toggleClass('open');
     });

     $('li.menu-item-has-children > a').on('click', function(e){
          e.preventDefault();
          $(this).parent().parent('ul').toggleClass('hidden');
          $(this).parent().toggleClass('show');

          if($(this).hasClass('lastMenu')) {
               $(this).removeClass('lastMenu');
               $(this).parent('li').parent('.sub-menu').parent('li').children('a').addClass('lastMenu');
          } else {
               $('.lastMenu').removeClass('lastMenu');
               $(this).addClass('lastMenu');
          }

          $(this).next('.sub-menu').toggleClass('show');
     });

     var sliderEbooks = $('.sliderEbooks').slick({
          prevArrow: $('section .slider .prev'),
          nextArrow: $('section .slider .next'),
          dots: false,
          infinite: true,
          slidesToShow: 1
     });

     var sliderEbooks = $('.sliderEspecialidades').slick({
          prevArrow: $('section .slider .prev'),
          nextArrow: $('section .slider .next'),
          dots: false,
          infinite: true,
          slidesToShow: 2
     });
     
});
     
$(window).scroll(function(){
     menu();
     scrolling();
});

$('body').bind('touchmove', function(e) { 

     //var scroll = $(window).scrollTop();
     var scroll = $(this).scrollTop()

     if ($(window).width() <= 992) {
          var scrollIn1 = $('section.paginas .item.fiv').offset().top - $('section.paginas .item.fiv').outerHeight();
          var scrollOut1 = $('section.paginas .item.fiv').offset().top;

          if ((scroll >= scrollIn1) && (scroll <= scrollOut1)) {
               $('section.paginas .item.fiv').addClass('animation');
          } else {
               $('section.paginas .item.fiv').removeClass('animation');
          }


          var scrollIn2 = $('section.paginas .item.endo').offset().top - $('section.paginas .item.endo').outerHeight();
          var scrollOut2 = $('section.paginas .item.endo').offset().top;

          if ((scroll >= scrollIn2) && (scroll <= scrollOut2)) {
               $('section.paginas .item.endo').addClass('animation');
          } else {
               $('section.paginas .item.endo').removeClass('animation');
          }


          var scrollIn3 = $('section.paginas .item.trombo').offset().top - $('section.paginas .item.trombo').outerHeight();
          var scrollOut3 = $('section.paginas .item.trombo').offset().top;

          if ((scroll >= scrollIn3) && (scroll <= scrollOut3)) {
               $('section.paginas .item.trombo').addClass('animation');
          } else {
               $('section.paginas .item.trombo').removeClass('animation');
          }


          var scrollIn4 = $('section.paginas .item.sop').offset().top - $('section.paginas .item.sop').outerHeight();
          var scrollOut4 = $('section.paginas .item.sop').offset().top;

          if ((scroll >= scrollIn4) && (scroll <= scrollOut4)) {
               $('section.paginas .item.sop').addClass('animation');
          } else {
               $('section.paginas .item.sop').removeClass('animation');
          }


          var scrollIn5 = $('section.paginas .item.varico').offset().top - $('section.paginas .item.varico').outerHeight();
          var scrollOut5 = $('section.paginas .item.varico').offset().top;

          if ((scroll >= scrollIn5) && (scroll <= scrollOut5)) {
               $('section.paginas .item.varico').addClass('animation');
          } else {
               $('section.paginas .item.varico').removeClass('animation');
          }


          var scrollIn6 = $('section.paginas .item.ovo').offset().top - $('section.paginas .item.ovo').outerHeight();
          var scrollOut6 = $('section.paginas .item.ovo').offset().top;

          if ((scroll >= scrollIn6) && (scroll <= scrollOut6)) {
               $('section.paginas .item.ovo').addClass('animation');
          } else {
               $('section.paginas .item.ovo').removeClass('animation');
          }


          var scrollIn7 = $('section.paginas .item.embri').offset().top - $('section.paginas .item.embri').outerHeight();
          var scrollOut7 = $('section.paginas .item.embri').offset().top;

          if ((scroll >= scrollIn7) && (scroll <= scrollOut7)) {
               $('section.paginas .item.embri').addClass('animation');
          } else {
               $('section.paginas .item.embri').removeClass('animation');
          }


          var scrollIn8 = $('section.paginas .item.social').offset().top - $('section.paginas .item.social').outerHeight();
          var scrollOut8 = $('section.paginas .item.social').offset().top;

          if ((scroll >= scrollIn8) && (scroll <= scrollOut8)) {
               $('section.paginas .item.social').addClass('animation');
          } else {
               $('section.paginas .item.social').removeClass('animation');
          }


          var scrollIn9 = $('section.paginas .item.femi').offset().top - $('section.paginas .item.femi').outerHeight();
          var scrollOut9 = $('section.paginas .item.femi').offset().top;

          if ((scroll >= scrollIn9) && (scroll <= scrollOut9)) {
               $('section.paginas .item.femi').addClass('animation');
          } else {
               $('section.paginas .item.femi').removeClass('animation');
          }


          var scrollIn10 = $('section.paginas .item.masc').offset().top - $('section.paginas .item.masc').outerHeight();
          var scrollOut10 = $('section.paginas .item.masc').offset().top;

          if ((scroll >= scrollIn10) && (scroll <= scrollOut10)) {
               $('section.paginas .item.masc').addClass('animation');
          } else {
               $('section.paginas .item.masc').removeClass('animation');
          }
     }
});