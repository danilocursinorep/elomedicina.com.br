<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Clínica_ELO
 */

get_header('home'); ?>


	<section class="dra">
		<div class="container">
			<div class="row justify-content-center">
				<div class="item col-3 col-md-4">
					<a href="<?php echo home_url('equipe#rosane'); ?>">
						<div class="conteudo">
							<h2><strong>Dra.</strong> Rosane Rodrigues</h2>
							<span>Conheça mais a especialista ></span>
						</div>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
					</a>
				</div>
				<div class="item col-3 col-md-4">
					<a href="<?php echo home_url('equipe#carla'); ?>">
						<div class="conteudo">
							<h2><strong>Dra.</strong> Carla Iaconelli</h2>
							<span>Conheça mais a especialista ></span>
						</div>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
					</a>
				</div>
				<div class="item col-3 col-md-4">
					<a href="<?php echo home_url('equipe#fernanda'); ?>">
						<div class="conteudo">
							<h2><strong>Dra.</strong> Fernanda Valente</h2>
							<span>Conheça mais a especialista ></span>
						</div>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="agendar">
		<div class="bg"></div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ondaHeader2.png" class="onda topo" />
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda1.png" class="onda desktop" />
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda1.png" class="onda" />
		<div class="formainer">
			<div class="container">
				<div class="row justify-content-center">
					<div class="form col-12	 align-self-center">
						<div class="conteudo">
							<h2>Pré-agendamento</h2>
						</div>
					</div>
					<div class="form col-12 col-lg-3 align-self-center">
						<div class="conteudo">
							<span>Preencha seus dados ao lado e a nossa equipe entrará em contato para efetuar o agendamento.</span>
						</div>
					</div>
					<div class="form col-12 col-lg-4 align-self-center">
						<div class="conteudo">
							<div class="bookly">

								<div id="mauticform_wrapper_preagendamentosite" class="mauticform_wrapper">
								    <form autocomplete="false" role="form" method="post" action="https://mkt.elomedicina.com.br/form/submit?formId=2" id="mauticform_preagendamentosite" data-mautic-form="preagendamentosite" enctype="multipart/form-data">
								        <div class="mauticform-error" id="mauticform_preagendamentosite_error"></div>
								        <div class="mauticform-message" id="mauticform_preagendamentosite_message"></div>
								        <div class="mauticform-innerform">

								            
								          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

								            <div id="mauticform_preagendamentosite_nome_completo" data-validate="nome_completo" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
								                <input id="mauticform_input_preagendamentosite_nome_completo" name="mauticform[nome_completo]" value="" placeholder="Nome completo" class="mauticform-input" type="text">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_preagendamentosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
								                <input id="mauticform_input_preagendamentosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_preagendamentosite_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
								                <input id="mauticform_input_preagendamentosite_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input" type="text">
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_preagendamentosite_selecione_a_medica" data-validate="selecione_a_medica" data-validation-type="select" class="mauticform-row mauticform-select mauticform-field-4 mauticform-required">
								                <select id="mauticform_input_preagendamentosite_selecione_a_medica" name="mauticform[selecione_a_medica]" value="" class="mauticform-selectbox">
								                    <option value="">Selecione a Médica</option>                    <option value="Dra. Carla Iaconelli">Dra. Carla Iaconelli</option>                    <option value="Dra. Fernanda Valente">Dra. Fernanda Valente</option>                    <option value="Dra. Rosane Rodrigues">Dra. Rosane Rodrigues</option>
								                </select>
								                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
								            </div>

								            <div id="mauticform_preagendamentosite_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
								                <button type="submit" name="mauticform[enviar]" id="mauticform_input_preagendamentosite_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
								            </div>
								            </div>
								        </div>

								        <input type="hidden" name="mauticform[formId]" id="mauticform_preagendamentosite_id" value="2">
								        <input type="hidden" name="mauticform[return]" id="mauticform_preagendamentosite_return" value="">
								        <input type="hidden" name="mauticform[formName]" id="mauticform_preagendamentosite_name" value="preagendamentosite">

								        </form>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="paginas">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/estetoscopioA.png" class="enfeite anima desktop" />
		<div class="container">
			<div class="row justify-content-center">
				<div class="titulo col-12">
					<h2>••• Reprodução humana em destaque •••</h2>
				</div>
				<div class="item fiv grande col-12 col-lg-9 align-self-center">
					<a href="<?php echo home_url('fertilizacao-in-vitro-fiv'); ?>">
					<div class="row">
						<div class="content col-12 col-lg-5 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/fiv.png" class="static big" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/fivA.png" class="anima big" />
						</div>
						<div class="col-12 col-lg-7 align-self-center">
							<h2><strong>Fertilização</strong> in vitro - FIV</h2>
							<p>Após um ano de tentativas de gravidez, o casal pode ser diagnosticado com infertilidade. A investigação do quadro pode detectar problemas femininos, masculinos ou de ambos. De acordo com o fator de infertilidade identificado, o especialista em reprodução assistida indica a (...)</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
				<div class="item endo col-12 col-lg-4 align-self-center">
					<a href="<?php echo home_url('endometriose'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/endometriose.png" class="static" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/endometrioseA.png" class="anima" />
						</div>
						<div class="col-12 align-self-center">
							<h2>Endometriose</h2>
							<p>A endometriose é uma doença ginecológica sem potencial de malignidade, mas que pode comprometer a função de importantes órgãos e afetar severamente a qualidade de vida da mulher.</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
				<div class="item trombo col-12 col-lg-4 align-self-center">
					<a href="<?php echo home_url('trombofilia'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/trombofilia.png" class="static" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/trombofiliaA.png" class="anima" />
						</div>
						<div class="col-12 align-self-center">
							<h2>Trombofilia</h2>
							<p>As trombofilias são desordens do sistema hemostático que levam a um estado de hipercoagulabilidade. Dessa forma, há uma predisposição para o desenvolvimento de coágulos, com risco aumentado (...)</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="item sop col-12 col-lg-4 align-self-top">
					<a href="<?php echo home_url('sindrome-dos-ovarios-policisticos-sop'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/sop.png" class="static" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/sopA.png" class="anima" />
						</div>
						<div class="col-12 align-self-center">
							<h2>SOP <strong>Síndrome dos ovários policísticos</strong></h2>
							<p>A síndrome dos ovários policísticos (SOP) é uma endocrinopatia complexa, que tem como características principais a presença de múltiplos cistos nos ovários, além de hiperandrogenismo e anovulação crônica.</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
				<div class="item varico col-12 col-lg-4 align-self-center">
					<a href="<?php echo home_url('varicocele'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/varicocele.png" class="static translate" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/varicoceleA.png" class="anima translate" />
						</div>
						<div class="col-12 align-self-center">
							<h2>Varicocele</h2>
							<p>A varicocele é caracterizada por uma dilatação nas veias do plexo pampiniforme, responsável pela drenagem sanguínea da bolsa testicular. Com essa alteração, o retorno venoso é prejudicado (...)</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="item ovo grande col-12 col-lg-9 align-self-center">
					<a href="<?php echo home_url('doacao-de-ovulos'); ?>">
					<div class="row">
						<div class="content col-12 col-lg-5 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/ovodoacao2.png" class="static" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/ovodoacao2A.png" class="anima" />
						</div>
						<div class="col-12 col-lg-7 align-self-center">
							<h2>Ovodoação</h2>
							<p>A doação de óvulos é uma importante técnica no âmbito da reprodução assistida, uma vez que possibilita a gestação para mulheres que não conseguem engravidar com seus próprios gametas.</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
				<div class="item embri col-12 col-lg-4 align-self-center">
					<a href="<?php echo home_url('transferencia-de-embrioes-congelados'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/transferencia.png" class="static" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/transferenciaA.png" class="anima" />
						</div>
						<div class="col-12 align-self-center">
						
							<h2>Transfrência de embriões congelados</h2>
							<p>A transferência de embriões congelados é uma alternativa nos tratamentos de fertilização in vitro (FIV). A técnica pode ser utilizada sempre que a transferência não puder ser realizada com embriões a fresco (...)</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
				<div class="item social col-12 col-lg-4 align-self-center">
					<a href="<?php echo home_url('preservacao-social-da-fertilidade'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/preservacao.png" class="static" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/preservacaoA.png" class="anima" />
						</div>
						<div class="col-12 align-self-center">
							<h2>Preservação social da fertilidade</h2>
							<p>A reprodução assistida reúne diferentes técnicas que viabilizam a gestação para casais inférteis. Além de tratar pacientes com quadros variados de infertilidade, as pessoas também podem (...)</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="item femi col-12 col-lg-4 align-self-center">
					<a href="<?php echo home_url('infertilidade-feminina'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/infertilidade_feminina.png" class="static" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/infertilidade_femininaA.png" class="anima" />
						</div>
						<div class="col-12 align-self-center">
							<h2>Infertilidade Feminina</h2>
							<p>Muitas mulheres têm o sonho de ser mãe, mas se deparam com a infertilidade. Isso pode acontecer por diversos fatores, isolados ou em conjunto, incluindo idade, hábitos de vida, irregularidades (...)</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
				<div class="item masc col-12 col-lg-4 align-self-center">
					<a href="<?php echo home_url('infertilidade-masculina'); ?>">
					<div class="row">
						<div class="content col-12 align-self-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/glow.png" class="glow" />
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/acons/infertilidade_masculina.png" class="static">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/infertilidade_masculinaA.png" class="anima">
						</div>
						<div class="col-12 align-self-center">
							<h2>Infertilidade Masculina</h2>
							<p>Por muito tempo, as dificuldades que um casal enfrentava para ter filhos eram associadas a problemas reprodutivos da mulher. Após anos de estudos na área, foi constatado que a infertilidade por fator (...)</p>
							<button>Saiba mais</button>
						</div>
					</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="equipe">
		<div class="grad topo desktop"></div>
		<div class="grad fundo"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="logo col-2 col-lg-2 align-self-center">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logopagina.png">
				</div>
				<div class="txt col-10 col-lg-6 align-self-center">
					<p>A clínica Elo tem o compromisso de oferecer uma medicina reprodutiva com acolhimento, atenção integral à saúde da mulher e atendimento multidisciplinar; com qualidade, segurança, ciência, tecnologia e empoderamento feminino.</p>
					<span>Uma equipe de mulheres para você, mulher, que deseja a gravidez.</span>
				</div>
			</div>
			<div class="row mm justify-content-center">
				<div class="medica col-4 align-self-center">
					<a href="<?php echo home_url('equipe#fernanda'); ?>">
						<h2><strong>Dra.</strong> Fernanda Valente</h2>
					</a>
				</div>
				<div class="medica col-4 align-self-center">
					<a href="<?php echo home_url('equipe#rosane'); ?>">
						<h2><strong>Dra.</strong> Rosane Rodrigues</h2>
					</a>
				</div>
				<div class="medica col-4 align-self-center">
					<a href="<?php echo home_url('equipe#carla'); ?>">
						<h2><strong>Dra.</strong> Carla Iaconelli</h2>
					</a>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="equipe col-12">
					<a href="<?php echo home_url('equipe'); ?>"><h2>Conheça toda a nossa equipe e clínica</h2></a>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
				</div>
			</div>
		</div>
	</section>
	<section class="agendar2">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda5.png" class="onda" />
		<div class="grad fundo mobile"></div>
		<div class="container">
			<div class="row justify-content-end">
				<div class="form col-12 col-lg-6 align-self-center">
					<div class="conteudo">
						<img class="desktop" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
						<h2>Pré-agendamento</h2>
						<span>Preencha seus dados abaixo e a nossa equipe entrará em contato para efetuar o agendamento.</span>
						<div class="bookly">
							<div id="mauticform_wrapper_preagendamento2site" class="mauticform_wrapper">
							    <form autocomplete="false" role="form" method="post" action="http://mkt.elomedicina.com.br/form/submit?formId=5" id="mauticform_preagendamento2site" data-mautic-form="preagendamento2site" enctype="multipart/form-data">
							        <div class="mauticform-error" id="mauticform_preagendamento2site_error"></div>
							        <div class="mauticform-message" id="mauticform_preagendamento2site_message"></div>
							        <div class="mauticform-innerform">

							            
							          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

							            <div id="mauticform_preagendamento2site_nome_completo" data-validate="nome_completo" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
							                <input id="mauticform_input_preagendamento2site_nome_completo" name="mauticform[nome_completo]" value="" placeholder="Nome completo" class="mauticform-input" type="text">
							                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
							            </div>

							            <div id="mauticform_preagendamento2site_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
							                <input id="mauticform_input_preagendamento2site_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
							                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
							            </div>

							            <div id="mauticform_preagendamento2site_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
							                <input id="mauticform_input_preagendamento2site_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input" type="text">
							                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
							            </div>

							            <div id="mauticform_preagendamento2site_selecione_a_medica" data-validate="selecione_a_medica" data-validation-type="select" class="mauticform-row mauticform-select mauticform-field-4 mauticform-required">
							                <select id="mauticform_input_preagendamento2site_selecione_a_medica" name="mauticform[selecione_a_medica]" value="" placeholder="Selecione a Médica" class="mauticform-selectbox">
							                    <option value="">Selecione a Médica</option>                    <option value="Dra. Carla Iaconelli">Dra. Carla Iaconelli</option>                    <option value="Dra. Fernanda Valente">Dra. Fernanda Valente</option>                    <option value="Dra. Rosane Rodrigues">Dra. Rosane Rodrigues</option>
							                </select>
							                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
							            </div>

							            <div id="mauticform_preagendamento2site_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
							                <button type="submit" name="mauticform[enviar]" id="mauticform_input_preagendamento2site_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
							            </div>
							            </div>
							        </div>

							        <input type="hidden" name="mauticform[formId]" id="mauticform_preagendamento2site_id" value="5">
							        <input type="hidden" name="mauticform[return]" id="mauticform_preagendamento2site_return" value="">
							        <input type="hidden" name="mauticform[formName]" id="mauticform_preagendamento2site_name" value="preagendamento2site">

							        </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php $query = new WP_Query(array( 
		'post_type' => 'post',
		'posts_per_page' => 3,
		'order' => 'DESC'
	)); ?>
	<?php if($query->have_posts()): ?>
	<section class="blog">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda6.png" class="onda" />
		<div class="container">
			<div class="row titulo justify-content-center">
				<div class="col-12 col-lg-3 align-self-center">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
					<h2>Blogposts</h2>
				</div>
				<div class="col-12 col-lg-4 align-self-center">
					<p>Para você ficar por dentro de diversos assuntos<br/> da Reprodução Assistida e da saúde integral feminina</p>
				</div>
			</div>
			
			<?php $postsBlog = array(); ?>
			<?php while($query->have_posts()): $query->the_post(); ?>
				<?php $index = $query->current_post; ?>

				<?php $postsBlog[$index]['title'] = get_the_title(); ?>
				<?php $postsBlog[$index]['permalink'] = get_the_permalink(); ?>
				<?php $postsBlog[$index]['excerpt'] = get_the_excerpt(); ?>
				<?php $postsBlog[$index]['image'] = get_the_post_thumbnail_url(); ?>

			<?php endwhile; ?>

			<div class="row itens justify-content-center">
				<div class="col-12 col-lg-6 align-self-end">
					<div class="img desktop" style="background-image: url(<?php echo $postsBlog[0]['image']; ?>);">
						<a href="<?php echo $postsBlog[0]['permalink']; ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/circulo.png">
						</a>
					</div>
				</div>
				<div class="col-10 col-lg-4 mobile">
					<a href="<?php echo $postsBlog[0]['permalink']; ?>">
						<div class="img" style="background-image: url(<?php echo $postsBlog[0]['image']; ?>);"></div>
						<h2><?php echo $postsBlog[0]['title']; ?></h2>
						<p><?php echo $postsBlog[0]['excerpt']; ?>.</p>
						<button>Saiba mais</button>
					</a>
				</div>
				<div class="col-10 col-lg-4 offset-lg-1">
					<a href="<?php echo $postsBlog[1]['permalink']; ?>">
						<div class="img" style="background-image: url(<?php echo $postsBlog[1]['image']; ?>);"></div>
						<h2><?php echo $postsBlog[1]['title']; ?></h2>
						<p><?php echo $postsBlog[1]['excerpt']; ?>.</p>
						<button>Saiba mais</button>
					</a>
				</div>
			</div>
			<div class="row itens justify-content-center">
				<div class="col-10 col-lg-3 desktop">
					<a href="<?php echo $postsBlog[0]['permalink']; ?>">
						<h2><?php echo $postsBlog[0]['title']; ?></h2>
						<p><?php echo $postsBlog[0]['excerpt']; ?>.</p>
						<button>Saiba mais</button>
					</a>
				</div>
				<div class="col-10 col-lg-4 offset-lg-1">
					<a href="<?php echo $postsBlog[2]['permalink']; ?>">
						<div class="img" style="background-image: url(<?php echo $postsBlog[2]['image']; ?>);"></div>
						<h2><?php echo $postsBlog[2]['title']; ?></h2>
						<p><?php echo $postsBlog[2]['excerpt']; ?>.</p>
						<button>Saiba mais</button>
					</a>
				</div>
			</div>
			<div class="row itens justify-content-center">
				<div class="col-12 mobile">
					<a class="mais" href="<?php echo home_url('blog'); ?>">veja mais blogposts</a>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php $query = new WP_Query(array( 
		'post_type' => 'ebook',
		'posts_per_page' => -1,
		'order' => 'DESC'
	)); ?>
	<div class="bgMobile<?php echo ((!$query->have_posts())?' center':''); ?>">
		<?php if($query->have_posts()): ?>
		<section class="ebooks">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/raw/acons/celularA.png" class="enfeite desktop" />
			<div class="container">
				<div class="row titulo justify-content-center">
					<div class="col-12 col-lg-3 align-self-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaHV.png">
						<h2>e-books</h2>
					</div>
					<div class="col-12 col-lg-4 align-self-center">
						<p>Assuntos aprofundados para esclarecer todas as<br/> suas dúvidas e ajudar em suas decisões.</p>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="slider col-8	 col-lg-10 align-self-center">
						<img class="seta prev" src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSlider.png">
						<img class="seta next" src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSlider.png">
						<div class="itens">
							<div class="sliderEbooks">
							<?php $postsBlog = array(); ?>
							<?php while($query->have_posts()): $query->the_post(); ?>
								<?php $index = $query->current_post; ?>
								<div class="item">
									<div>
										<div class="row">
											<div class="col-12 col-lg-6 align-self-center">
												<a href="<?php the_permalink(); ?>">
													<?php the_post_thumbnail(); ?>
												</a>
											</div>
											<div class="col-12 col-lg-6 align-self-center">
												<a href="<?php the_permalink(); ?>">
													<h2><?php the_title(); ?></h2>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus gravida vehicula massa, non tempor metus. Praesent quis augue ornare, vulputate purus aliqua.</p>
													<button><img src="<?php echo get_template_directory_uri(); ?>/assets/img/setaBT.png"> Baixe gratuitamente!</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							<?php endwhile ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php endif; ?>
		<section class="newsletter">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
			<div class="container">
				<div class="row titulo justify-content-center justify-content-lg-start">
					<div class="col-10 col-lg-4 offset-lg-2 align-self-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaHV.png">
						<h2>Newsletter</h2>
						<p>Assuntos aprofundados para esclarecer todas as<br/> suas dúvidas e ajudar em suas decisões.</p>

						<div id="mauticform_wrapper_newsletter" class="mauticform_wrapper">
						    <form autocomplete="false" role="form" method="post" action="https://mkt.elomedicina.com.br/form/submit?formId=4" id="mauticform_newsletter" data-mautic-form="newsletter" enctype="multipart/form-data" class="everyForm">
						        <div class="mauticform-error" id="mauticform_newsletter_error"></div>
						        <div class="mauticform-message" id="mauticform_newsletter_message"></div>
						        <div class="mauticform-innerform">

						            
						          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

						            <div id="mauticform_newsletter_nome_completo" data-validate="nome_completo" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
						                <input id="mauticform_input_newsletter_nome_completo" name="mauticform[nome_completo]" value="" placeholder="Nome completo" class="mauticform-input" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_newsletter_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
						                <input id="mauticform_input_newsletter_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_newsletter_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-3">
						                <button type="submit" name="mauticform[submit]" id="mauticform_input_newsletter_submit" value="" class="mauticform-button btn btn-default">Enviar</button>
						            </div>
						            </div>
						        </div>

						        <input type="hidden" name="mauticform[formId]" id="mauticform_newsletter_id" value="4">
						        <input type="hidden" name="mauticform[return]" id="mauticform_newsletter_return" value="">
						        <input type="hidden" name="mauticform[formName]" id="mauticform_newsletter_name" value="newsletter">

						        </form>
						</div>

					</div>
				</div>
			</div>
		</section>
	</div>

     <div class="modal fade modalagendamento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
               <div class="modal-content">
                    <img class="button" src="<?php echo get_template_directory_uri(); ?>/assets/img/close.png" data-dismiss="modal" aria-label="Close">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/popupagendamento.png">
               </div>
          </div>
     </div>

<?php get_footer();