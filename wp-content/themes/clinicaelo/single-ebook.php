<?php get_header('ebook'); ?>
     <?php while (have_posts()): the_post(); ?>
     <section class="conteudoEbook">
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12 col-lg-8">
                         <h2><?php the_field('descricao_curta'); ?></h2>
                         <div class="row">
                              <div class="col-12 col-lg-6">
                                   <?php the_field('descricao'); ?>
                              </div>
                              <div class="col-12 col-lg-6">
                                   <div class="form">
                                        <?php the_field('formulario'); ?>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <div class="share col-12 col-lg-8">
                         <h4><strong>Compartilhe :-)</strong></h4>
                         <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
                              <img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaFB.png">
                         </a>
                         <a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>">
                              <img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaTwitter.png">
                         </a>
                         <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=">
                              <img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaLinkedin.png">
                         </a>
                         <a target="_blank" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>">
                              <img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaWhats.png">
                         </a>
                    </div>
               </div>
          </div>
     </section>
     <?php endwhile; ?>
<?php get_footer();