<?php get_header(); ?>

     <section class="frase mobile">
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12">
                         <p>Assuntos aprofundados para esclarecer todas as suas dúvidas da reprodução assistida e ajudar em suas decisões.</p>
                    </div>
               </div>
          </div>
     </section>
     
     <?php if (have_posts()): ?>
     <section class="bookposts">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
          <div class="grad"></div>
          <div class="container">
               <div class="row itens justify-content-center">
                    <div class="itens col-12 col-lg-8 align-self-start">
                         <?php while(have_posts()): the_post(); ?>
                              <div class="row item">
                                   <div class="col-12">
                                        <h2><?php the_title(); ?></h2>
                                   </div>
                                   <div class="col-12 col-lg-6 align-self-center">
                                        <p><?php the_post_thumbnail(); ?></p>
                                   </div>
                                   <div class="col-12 col-lg-6 align-self-center descricao_curta">
                                        <?php the_field('descricao_curta'); ?>
                                   </div>
                                   <div class="col-12 descricao">
                                        <?php the_field('descricao'); ?>
                                   </div>
                                   <div class="col-12 text-right">
                                        <a href="<?php the_permalink(); ?>" target="_self">
                                             <button><strong>&#9650;</strong> Baixe gratuitamente</button>
                                        </a>
                                   </div>
                              </div>
                         <?php endwhile; ?>
                    </div>
               </div>
          </div>
     </section>
     <?php else: ?>
          <?php header('Location:' . home_url()); ?>
     <?php endif; ?>
 
<?php get_footer();