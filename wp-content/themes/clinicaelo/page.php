<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Clínica_ELO
 */

get_header(); ?>

	<section class="especialidade">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
				<?php while (have_posts()):
					the_post();
					get_template_part( 'template-parts/content', 'page' );
				endwhile; ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();