<?php get_header(); ?>

	<section class="contato">
		<div class="grad"></div>
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-12 col-lg-8">
					<h2>Queremos ajudar você a realizar seu sonho.<br> Conheça os nossos canais de comunicação:</h2>
					<div class="row contatos">
						<div class="col-12">
							<a href="tel:+551138059754" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeTel.png"> <span>(11) 3805-9754</span>
							</a>
						</div>
						<div class="col-12">
							<a href="https://api.whatsapp.com/send?phone=5511940076113" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeWhats.png"><span>(11) 94007-6113</span>
							</a>
						</div>
						<div class="col-12">
							<a href="mailto:contato@elomedicina.com.br" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png"><span>contato@elomedicina.com.br</span>
							</a>
						</div>
						<div class="col-12">
							<a href="https://goo.gl/maps/MLoXuvypLbkwTuPC8" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeMaps.png"><span>Alameda dos Maracatins Nº 426 • Conjunto 804<br/> Moema, São Paulo/SP CEP 04089-010</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row itens justify-content-center">
				<div class="col-12 col-lg-8">
					<h2>Você tem alguma dúvida, fale conosco:</h2>
					<div id="mauticform_wrapper_contatosite" class="mauticform_wrapper">
					    <form autocomplete="false" role="form" method="post" action="http://mkt.elomedicina.com.br/form/submit?formId=1" id="mauticform_contatosite" data-mautic-form="contatosite" enctype="multipart/form-data">
					        <div class="mauticform-error" id="mauticform_contatosite_error"></div>
					        <div class="mauticform-message" id="mauticform_contatosite_message"></div>
					        <div class="mauticform-innerform">

					            
					          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

					            <div id="mauticform_contatosite_nome_completo" data-validate="nome_completo" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
					                <input id="mauticform_input_contatosite_nome_completo" name="mauticform[nome_completo]" value="" placeholder="Nome completo" class="mauticform-input" type="text">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>

					            <div id="mauticform_contatosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
					                <input id="mauticform_input_contatosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>

					            <div id="mauticform_contatosite_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
					                <input id="mauticform_input_contatosite_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input" type="text">
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>

					            <div id="mauticform_contatosite_sua_mensagm" data-validate="sua_mensagm" data-validation-type="textarea" class="mauticform-row mauticform-text mauticform-field-4 mauticform-required">
					                <textarea id="mauticform_input_contatosite_sua_mensagm" name="mauticform[sua_mensagm]" class="mauticform-textarea"></textarea>
					                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
					            </div>

					            <div id="mauticform_contatosite_submit" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
					                <button type="submit" name="mauticform[submit]" id="mauticform_input_contatosite_submit" value="" class="mauticform-button btn btn-default">Enviar</button>
					            </div>
					            </div>
					        </div>

					        <input type="hidden" name="mauticform[formId]" id="mauticform_contatosite_id" value="1">
					        <input type="hidden" name="mauticform[return]" id="mauticform_contatosite_return" value="">
					        <input type="hidden" name="mauticform[formName]" id="mauticform_contatosite_name" value="contatosite">

					        </form>
					</div>
				</div>
				<div class="share col-12 col-lg-8">
					<h4><strong>A Clínica Elo também<br/> está nas redes sociais:</strong></h4>
					<a target="_blank" href="www.facebook.com/elo.medicina">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaFB.png">
					</a>
					<a target="_blank" href="https://www.instagram.com/elo.medicina/">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaInsta.png">
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="ondas">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
	</section>


<?php get_footer();