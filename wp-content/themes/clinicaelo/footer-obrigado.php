<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clínica_ELO
 */

?>	
	</main><!-- #main -->
	<footer id="colophon" class="site-footer obrigado">
		<div class="container">
			<div class="row justify-content-center">
				<div class="logo col-12 col-lg-8">
					<a href="<?php echo home_url(); ?>" target="_self">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logoFooter.png" class="logo" />
					</a>
					<p><?php echo date('Y'); ?> &copy; Todos os direitos reservados. O conteúdo deste site foi elaborado pela equipe da Clínica Elo Medicina Reprodutiva e as informações aqui contidas tem caráter meramente informativo e educacional. Não deve ser utilizado para realizar autodiagnóstico ou automedicação. Em caso de dúvidas, consulte seu médico, somente ele está habilitado a praticar o ato médico, conforme recomendação do Conselho Federal de Medicina. Todas imagens contidas no site são meramente ilustrativas e foram compradas em banco de imagens, não envolvendo imagens de pacientes. Diretora Técnica: Dra. XXXXXXXXXXX - CRM 000-000.</p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
