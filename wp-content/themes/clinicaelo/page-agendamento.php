<?php get_header(); ?>

     <div class="modal fade modalagendamento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered">
               <div class="modal-content">
                    <img class="button" src="<?php echo get_template_directory_uri(); ?>/assets/img/close.png" data-dismiss="modal" aria-label="Close">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/popupagendamento.png">
               </div>
          </div>
     </div>

     <section class="frase mobile">
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12">
					<p>O primeiro passo para a realização de seu sonho.</p>
                    </div>
               </div>
          </div>
     </section>


	<section class="agendamento">
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-12 col-lg-8 align-self-center">
					<h2>Agende sua consuta aqui.<br/>
					Atendimento em reprodução assistida e saúde integral da mulher.<br> Conte com o nosso time de profissionais.</h2>

					<h3>Preencha seus dados e a nossa equipe entrará em contato para efetuar o agendamento.</h3>
					
					<div class="bookly">
						<div id="mauticform_wrapper_preagendamentosite" class="mauticform_wrapper">
						    <form autocomplete="false" role="form" method="post" action="http://mkt.elomedicina.com.br/form/submit?formId=2" id="mauticform_preagendamentosite" data-mautic-form="preagendamentosite" enctype="multipart/form-data">
						        <div class="mauticform-error" id="mauticform_preagendamentosite_error"></div>
						        <div class="mauticform-message" id="mauticform_preagendamentosite_message"></div>
						        <div class="mauticform-innerform">

						            
						          <div class="mauticform-page-wrapper mauticform-page-1" data-mautic-form-page="1">

						            <div id="mauticform_preagendamentosite_nome_completo" data-validate="nome_completo" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-1 mauticform-required">
						                <input id="mauticform_input_preagendamentosite_nome_completo" name="mauticform[nome_completo]" value="" placeholder="Nome completo" class="mauticform-input" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_preagendamentosite_email" data-validate="email" data-validation-type="email" class="mauticform-row mauticform-email mauticform-field-2 mauticform-required">
						                <input id="mauticform_input_preagendamentosite_email" name="mauticform[email]" value="" placeholder="E-mail" class="mauticform-input" type="email">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_preagendamentosite_celular" data-validate="celular" data-validation-type="text" class="mauticform-row mauticform-text mauticform-field-3 mauticform-required">
						                <input id="mauticform_input_preagendamentosite_celular" name="mauticform[celular]" value="" placeholder="Celular" class="mauticform-input" type="text">
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_preagendamentosite_selecione_a_medica" data-validate="selecione_a_medica" data-validation-type="select" class="mauticform-row mauticform-select mauticform-field-4 mauticform-required">
						                <select id="mauticform_input_preagendamentosite_selecione_a_medica" name="mauticform[selecione_a_medica]" value="" placeholder="Selecione a Médica" class="mauticform-selectbox">
						                    <option value="">Selecione a Médica</option>                    <option value="Dra. Carla Iaconelli">Dra. Carla Iaconelli</option>                    <option value="Dra. Fernanda Valente">Dra. Fernanda Valente</option>                    <option value="Dra. Rosane Rodrigues">Dra. Rosane Rodrigues</option>
						                </select>
						                <span class="mauticform-errormsg" style="display: none;">Esse campo é obrigatório!</span>
						            </div>

						            <div id="mauticform_preagendamentosite_enviar" class="mauticform-row mauticform-button-wrapper mauticform-field-5">
						                <button type="submit" name="mauticform[enviar]" id="mauticform_input_preagendamentosite_enviar" value="" class="mauticform-button btn btn-default">Enviar</button>
						            </div>
						            </div>
						        </div>

						        <input type="hidden" name="mauticform[formId]" id="mauticform_preagendamentosite_id" value="2">
						        <input type="hidden" name="mauticform[return]" id="mauticform_preagendamentosite_return" value="">
						        <input type="hidden" name="mauticform[formName]" id="mauticform_preagendamentosite_name" value="preagendamentosite">

						        </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ondas">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
	</section>

<?php get_footer();