<?php //Template Name: Especialidades ?>
<?php get_header(); ?>

	<?php while(have_posts()): ?>
	<?php the_post(); ?>
	<section class="post">
		<div class="grad"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-9">
					<img class="linha" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
				</div>
				<div class="col-12 col-lg-8">
					<h3>Por <?php the_author_meta('user_firstname'); ?> <?php the_author_meta('user_lastname'); ?></h3>
				</div>
				<div class="content col-12 col-lg-8">
					<?php the_content(); ?>
				</div>
				<div class="col-12 col-lg-9">
					<img class="linha" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
				</div>
				<div class="share col-12 col-lg-8">
					<h4>Gostou desse artigo? <strong>Compartilhe :-)</strong></h4>
					<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaFB.png">
					</a>
					<a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaTwitter.png">
					</a>
					<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaLinkedin.png">
					</a>
					<a target="_blank" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaWhats.png">
					</a>
				</div>
				<div class="comments col-12 col-lg-8">
					<?php if (comments_open() || get_comments_number()): ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<?php endwhile; ?>
	
	<?php if (have_rows('itens')): ?>
	<section class="tambem">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
		<div class="grad"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="slider col-12 col-lg-8 align-self-center">
					<h2>Você pode gostar também:</h2>
					<img class="seta prev" src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSlider.png">
					<img class="seta next" src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSlider.png">
					<div class="itens">
						<div class="sliderEspecialidades">
							<?php while (have_rows('itens')): the_row(); ?>
							<?php $link = get_sub_field('item'); ?>
							<div class="item">
								<div class="row justify-content-center">
									<div class="col-12 col-lg-6 align-self-center">
										<a href="<?php echo $link['url']; ?>">
											<?php echo get_the_post_thumbnail(page_by_title($link['title'])); ?>
										</a>
									</div>
									<div class="col-12 col-lg-6 align-self-center">
										<a href="<?php echo $link['url']; ?>">
											<h3><?php echo $link['title']; ?></h3>
											<button>Saiba mais ></button>
										</a>
									</div>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<section class="ondas">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
	</section>

<?php get_footer();