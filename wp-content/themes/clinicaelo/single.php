<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Clínica_ELO
 */

get_header(); ?>

	<?php while(have_posts()): ?>
	<?php the_post(); ?>
	<section class="post">
		<div class="grad"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 col-lg-9">
					<img class="linha" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
				</div>
				<div class="col-12 col-lg-8">
					<h3>Por <?php the_author_meta('user_firstname'); ?> <?php the_author_meta('user_lastname'); ?></h3>
				</div>
				<div class="content col-12 col-lg-8">
					<?php the_content(); ?>
				</div>
				<div class="col-12 col-lg-9">
					<img class="linha" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
				</div>
				<div class="share col-12 col-lg-8">
					<h4>Gostou desse blogpost? <strong>Compartilhe :-)</strong></h4>
					<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaFB.png">
					</a>
					<a target="_blank" href="https://twitter.com/home?status=<?php the_permalink(); ?>">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaTwitter.png">
					</a>
					<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=&summary=&source=">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaLinkedin.png">
					</a>
					<a target="_blank" href="https://api.whatsapp.com/send?text=<?php the_permalink(); ?>">
						<img class="palheta" src="<?php echo get_template_directory_uri(); ?>/assets/img/palhetaWhats.png">
					</a>
				</div>
				<div class="comments col-12 col-lg-8">
					<?php if (comments_open() || get_comments_number()): ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<?php endwhile; ?>
	
	<section class="tambem">
		<div class="grad"></div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="slider col-12 col-lg-8 align-self-center">
					<h2>Você pode gostar também:</h2>
					<img class="seta prev" src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSlider.png">
					<img class="seta next" src="<?php echo get_template_directory_uri(); ?>/assets/img/setaSlider.png">
					<div class="itens">
						<div class="sliderEbooks">
							<div class="item">
								<div>
									<div class="row justify-content-center">
										<div class="col-12 col-lg-3 align-self-center">
											<img src="https://via.placeholder.com/1200x1000/F0F/000/?text=TESTE)">
										</div>
										<div class="col-12 col-lg-3 align-self-center">
											<h3>Título do livro ebook</h3>
											<button>Saiba mais ></button>
										</div>
										<div class="col-12 col-lg-3 align-self-center">
											<img src="https://via.placeholder.com/1200x1000/F0F/000/?text=TESTE)">
										</div>
										<div class="col-12 col-lg-3 align-self-center">
											<h3>Título do livro ebook</h3>
											<button>Saiba mais ></button>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div>
									<div class="row justify-content-center">
										<div class="col-12 col-lg-3 align-self-center">
											<img src="https://via.placeholder.com/1200x1000/F0F/000/?text=TESTE)">
										</div>
										<div class="col-12 col-lg-3 align-self-center">
											<h3>Título do livro ebook</h3>
											<button>Saiba mais ></button>
										</div>
										<div class="col-12 col-lg-3 align-self-center">
											<img src="https://via.placeholder.com/1200x1000/F0F/000/?text=TESTE)">
										</div>
										<div class="col-12 col-lg-3 align-self-center">
											<h3>Título do livro ebook</h3>
											<button>Saiba mais ></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();