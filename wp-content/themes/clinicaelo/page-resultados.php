<?php get_header(); ?>
	<script type="text/javascript">
		window.scrollTo({ top: 0 });

		$(window).scroll(function(){
		     scrolling();
		     if ($(this).scrollTop() > 30)  {
		          $('.scrollTop').addClass('show');
		     } else {
		          $('.scrollTop').removeClass('show');
		     }
		}
	</script>
	<section class="resultados">
		<img class="scrollTop" src="<?php echo get_template_directory_uri(); ?>/assets/img/setaBT.png" />
		<div class="grad"></div>
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-12">

					<h2 class="titulo">Tratamentos realizados com as especialistas:</h2>
					<h3 class="titulo">Dra. Carla A. R. Iaconelli, Dra. Fernanda M. Valente e Dra. Rosane S. Rodrigues</h3>

					<ul class="nav nav-tabs" id="Abas" role="tablist">
						
						<li class="nav-item ativo">
							<a class="nav-link active" id="resultados-tab" data-toggle="tab" href="#resultados" role="tab" aria-controls="resultados" aria-selected="true">Resultados ICSI - ANO 2019</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link" id="embrioes-tab" data-toggle="tab" href="#embrioes" role="tab" aria-controls="embrioes" aria-selected="false">Embriões criopreservados - ANO 2019</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link" id="criopreservados-tab" data-toggle="tab" href="#criopreservados" role="tab" aria-controls="criopreservados" aria-selected="false">ICSI + Criopreservados</a>
						</li>

					</ul>
				</div>
				<div class="col-12 col-lg-8">
					<div class="tab-content" id="Resultado">

						<div class="tab-pane fade show active" id="resultados" role="tabpanel" aria-labelledby="resultados-tab">
							<div class="informacao info2">
								<!-- <div class="item item1">
									<h2>Nº de <strong>ciclos realizados</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w85">148</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone1.png" class="icone" />
								</div> -->
								<div class="item item2">
									<h2><strong>Idade</strong> média da paciente:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w90">37.6</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone2.png" class="icone" />
								</div>
								<div class="item item3">
									<h2>Nº médio de <strong>unidades de FSH</strong> (UI):</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w119">2980</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone3.png" class="icone" />
								</div>
								<div class="item item4">
									<h2>Nº médio de <strong>folículos puncionados</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w94">11.4</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone4.png" class="icone" />
								</div>
								<div class="item item5">
									<h2>Nº médio de <strong>oócitos recuperados</strong><br> (para ICSI ou criopreservacão)</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w69">8.1</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone5.png" class="icone h" />
								</div>
								<div class="item item6">
									<h2>Nº médio de <strong>oócitos injetados</strong> (para ICSI):</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w71">4.9</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone6.png" class="icone" />
								</div>
								<div class="item item7">
									<h2>Taxa de <strong>fertilização normal</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">83</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaFert" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone7.png" class="icone" />
								</div>
								<div class="item item8">
									<h2>Nº médio de <strong>embriões transferidos</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w65">1.7</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone8.png" class="icone h" />
								</div>
								<div class="item item9">
									<h2>Porcentagem <strong>ciclos transferidos em Dia 5</strong>: </h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">72</h3>
									<h3 class="unidade">%</h3>
									<canvas id="CliclosTrans" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone8.png" class="icone h" />
								</div>
								<div class="item item10">
									<h2><strong>Taxa de gestação</strong> / <strong>ciclo transferido</strong>: </h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">40</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaGest" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone7.png" class="icone" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone1.png" class="icone" />
								</div>
								<div class="item item11">
									<h2><strong>Taxa gestação</strong> / <strong>ciclo transferido</strong> em <strong>D5</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">44.4</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaGestCiclo" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone7.png" class="icone" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone8.png" class="icone h" />
									<h4>D5</h4>
								</div>
								<div class="item item12">
									<h2><strong>Taxa implantação</strong> / <strong>embrião transferido:</strong></h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">25.4</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaImp" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone10.png" class="icone" />
								</div>
								<div class="item item13">
									<h2><strong>Taxa de implantação</strong> / <strong>embrião transferido</strong> em <strong>D5</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">26.7</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaImpD5" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone9.png" class="icone" />
									<h4>D5</h4>
								</div>
								<div class="item item14">
									<h2>Taxa de <strong>aborto</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">12.5</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaAbo" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone11.png" class="icone" />
								</div>
								<div class="item item15">
									<h2>Taxa de <strong>aborto</strong> / <strong>ciclo transferido em D5</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">16.7</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaAboD5" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone11.png" class="icone" />
									<h4>D5</h4>
								</div>
								<div class="item item16">
									<h2>Porcentagem <strong>ciclos transferidos com CRIO de embriões excedentes</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">24</h3>
									<h3 class="unidade">%</h3>
									<canvas id="CliclosTransEx" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone12.png" class="icone" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone13.png" class="icone h" />
								</div>
								<div class="item item17">
									<h2>Porcentagem ciclos <strong>não transferidos com CRIO de todos os embriões</strong>:</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">51.2</h3>
									<h3 class="unidade">%</h3>
									<canvas id="CiclosNao" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone9.png" class="icone" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone12.png" class="icone" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone14.png" class="icone" />
								</div>

							</div>
						</div>
						
						<div class="tab-pane fade" id="embrioes" role="tabpanel" aria-labelledby="embrioes-tab">
							<div class="informacao info1">
								<div class="item item18">
									<h2 class="blue">Nº de <strong>ciclos realizados:</strong></h2>
									<h3 class="zero">0</h3>
									<h3 class="valor w60">56</h3>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone1.png" class="icone" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone12.png" class="icone h" />
								</div>
								<div class="item item19">
									<h2 class="blue">Taxa <strong>gestação / ciclo transferido</strong></h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">63.3</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaGestTrans" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone7.png" class="icone" />
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone8.png" class="icone h" />
								</div>
								<div class="item item20">
									<h2 class="blue">Taxa <strong>implantação / embrião transferido</strong></h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">50.6</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaImpTrans" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone8.png" class="icone h" />
								</div>
								<div class="item item21">
									<h2 class="blue">Taxa de <strong>aborto</strong></h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">10.3</h3>
									<h3 class="unidade">%</h3>
									<canvas id="TaxaAboBlue" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone11.png" class="icone h" />
								</div>
							</div>
						</div>

						<div class="tab-pane fade" id="criopreservados" role="tabpanel" aria-labelledby="resultados-tab">
							<div class="informacao info1">
								<div class="item item22">
									<h2 class="verm">Taxa de <strong>gestação GERAL (ICSI + CRIO) / ciclo transferido</strong> para o período</h2>
									<h3 class="zero">0</h3>
									<h3 class="valor">54.4</h3>
									<h3 class="unidade">%</h3>
									<canvas id="GestGer" width="75" height="75"></canvas>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone6.png" class="icone h" />
									<span>+</span>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/img/resultados/icone12.png" class="icone h" />
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="ondas">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
	</section>

<?php get_footer();