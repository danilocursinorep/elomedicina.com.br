<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Clínica_ELO
 */

get_header('404'); ?>


	<section class="page404">
		<div class="grad"></div>
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-12 col-lg-8">
					<h2>Gostaria de tentar a pesquisa novamente?</h2>
					<?php get_search_form('page'); ?>
					<h2 class="margin">Também recomendados alguns conteúdos<br> do site que você pode gostar:</h2>
					<h3>Destaques</h3>
					<p>Nossos conteúdos mais acessados: </p>
					<?php $query = new WP_Query(array( 
						'post_type' => 'page',
						'posts_per_page' => 6,
						'ordeby' => 'rand',
						'order' => 'DESC'
					)); ?>
					<ul>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<li><?php the_title(); ?></li>
					<?php endwhile; ?>
					</ul>
					<div class="row justify-content-center">
						<?php $query = new WP_Query(array( 
							'post_type' => 'post',
							'posts_per_page' => 1,
							'order' => 'DESC'
						)); ?>
						<?php if($query->have_posts()): ?>
						<div class="col-12 col-lg-5">
							<h3>Blogposts</h3>
							<p>Nosso texto mais recente.</p>
							<?php while($query->have_posts()): $query->the_post(); ?>
								<div style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img round"></div>
								<h2><?php the_title(); ?></h2>
							<?php endwhile; ?>
							<button>> Conheça todos os e-books</button>
						</div>
						<?php endif; ?>
						<?php $query = new WP_Query(array( 
							'post_type' => 'ebook',
							'posts_per_page' => 1,
							'order' => 'DESC'
						)); ?>
						<?php if($query->have_posts()): ?>
						<div class="col-12 col-lg-5 offset-lg-2">
							<h3>e-Books</h3>
							<p>Nosso livro digital mais novo.</p>
							<?php while($query->have_posts()): $query->the_post(); ?>
								<div class="img"><?php the_post_thumbnail(); ?></div>
								<h2><?php the_title(); ?></h2>
							<?php endwhile; ?>
							<button>> Conheça todos os e-books</button>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();