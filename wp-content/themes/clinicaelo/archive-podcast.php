<?php get_header(); ?>

     <section class="frase mobile">
          <div class="container">
               <div class="row justify-content-center">
                    <div class="col-12">
                         <p>O podcast da Clínica Elo. Aqui você vai encontrar e escutar muitas informações sobre a medicina reprodutiva, com ciência, acolhimento e a experiências de nossas especialistas. </p>
                    </div>
               </div>
          </div>
     </section>

     <?php global $wp_query;  ?>
     <?php $index = $wp_query->found_posts; ?>

     <?php if (have_posts()): ?>
     <section class="podposts">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
          <div class="grad"></div>
          <div class="container">
               <div class="row itens justify-content-center">
                    <div class="itens col-12 col-lg-8 align-self-start">
                         <?php while(have_posts()): the_post(); ?>
                              <div class="row item justify-content-center">
                                   <div class="col-12 col-sm-8 col-lg-4 align-self-center text-center">
                                        <a href="https://podcast.elomedicina.com.br/" target="_blank">
                                             <h3 class="mobile">Episódio <?php echo $index; ?></h3>
                                             <h2 class="mobile"><?php the_title();?></h2>
                                             <?php the_post_thumbnail('thumb'); ?>
                                        </a>
                                   </div>
                                   <div class="col-12 col-lg-8">
                                        <a href="https://podcast.elomedicina.com.br/" target="_blank">
                                             <h3 class="desktop">Episódio <?php echo $index; ?></h3>
                                             <h2 class="desktop"><?php the_title();?></h2>
                                        </a>
                                        <div class="row">
                                             <?php if (get_field('link_ouvir')): ?>
                                             <div class="col-6 col-lg-4">
                                                  <div class="acao">
                                                       <a href="<?php the_field('link_ouvir'); ?>" target="_blank">
                                                            <h4>Escute
                                                            <img class="O" src="<?php echo get_template_directory_uri(); ?>/assets/img/icOuvir.png" />
                                                            </h4>
                                                       </a>
                                                  </div>
                                             </div>
                                             <?php endif; ?>
                                             <?php if (get_field('link_assistir')): ?>
                                             <div class="col-6 col-lg-4">
                                                  <div class="acao">
                                                       <a href="<?php the_field('link_assistir'); ?>" target="_blank">
                                                            <h4>Assista
                                                            <img class="A" src="<?php echo get_template_directory_uri(); ?>/assets/img/icAssistir.png" />
                                                            </h4>
                                                       </a>
                                                  </div>
                                             </div>
                                             <?php endif; ?>
                                        </div>
                                   </div>
                              </div>
                              <?php $index--; ?>
                         <?php endwhile; ?>
                    </div>
               </div>
          </div>
     </section>
     <?php else: ?>
          <?php header('Location:' . home_url()); ?>
     <?php endif; ?>
 
<?php get_footer();