<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clínica_ELO
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Facebook Pixel Code --> 
	<script> 
	!function(f,b,e,v,n,t,s) 
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod? 
	n.callMethod.apply(n,arguments):n.queue.push(arguments)}; 
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0'; 
	n.queue=[];t=b.createElement(e);t.async=!0; 
	t.src=v;s=b.getElementsByTagName(e)[0]; 
	s.parentNode.insertBefore(t,s)}(window, document,'script', 
	'https://connect.facebook.net/en_US/fbevents.js'); 
	fbq('init', '1333724026971352'); 
	fbq('track', 'PageView'); 
	</script> 
	<noscript><img height="1" width="1" style="display:none" 
	src="https://www.facebook.com/tr?id=1333724026971352&ev=PageView&noscript=1" 
	/></noscript> 
	<!-- End Facebook Pixel Code -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-178666846-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-178666846-1');
	</script>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon/site.webmanifest">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>
</head>
<?php global $post; ?>
<?php $slug = $post->post_name; ?>
<body <?php body_class($slug); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	<header id="masthead" class="site-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/ondaTopo.png" class="onda topo" />
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
		<div class="floatMenu">
			<div class="container">
				<div class="row">
					<div id="logo" class="col-6 align-self-center">
						<a href="<?php echo home_url(); ?>" target="_self">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" class="logo" />
						</a>
					</div>
					<nav id="site-navigation" class="col-6 col-lg-12 align-self-center main-navigation order-md-1">
	                        	<div id="menu-icone">
	                            <span></span>
	                            <span></span>
	                            <span></span>
	                            <span></span>
	                        </div>
						<div class="menu-container">
							<?php wp_nav_menu(
								array(
									'theme_location' => 'menu-1',
									'menu_id' => 'primary-menu'
								)
							); ?>
						</div>
					</nav>
				</div>
			</div>
		</div>
		<div class="contentTop">
			<div class="container">
				<div class="row">
					<div class="titulo blog col-12 col-lg-4 align-self-end">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
						<h2>Logo você receberá seu e-book no e-mail cadastrado ;-)</h2>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	<main id="main" class="site-main">
		<?php wpb_set_post_views(get_the_ID()); ?> 