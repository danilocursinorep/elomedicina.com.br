<?php get_header(); ?>

	<section class="frase mobile">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<p>Conheça os tratamentos oferecidos pela Elo Medicina, com toda a expertise e a estrutura da medicina reprodutiva para você ser bem acolhida.</p>
				</div>
			</div>
		</div>
	</section>

	<?php while(have_posts()): ?>
	<?php the_post(); ?>

	<section class="tratamentos">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
		<div class="grad"></div>
		<div class="container">

			<div class="row justify-content-center">
				<div class="col-12 col-lg-8">
					<div class="row itens justify-content-center">
						
						<a href="<?php echo home_url('como-descobrir-se-sou-infertil'); ?>" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone1.png" />
							<h2>Infertilidade</h2>
							<p>A investigação e os tratamentos das causas da infertilidade dos parceiros. Tudo para encontrar o caminho de maior taxa de sucesso para a sonhada gravidez</p>
							<button>Saiba mais</button>
						</a>
						
						<a href="<?php echo home_url('congelamento-de-ovulos'); ?>" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone2.png" />
							<h2>Avaliacão: <strong>Pré-congelamento <br>de óvulos</strong></h2>
							<p>Os exames e os procedimentos para o preparo dos gametas femininos para a criopreservação</p>
							<button>Saiba mais</button>
						</a>
						
						<a href="<?php echo home_url('avaliacao-da-reserva-ovariana'); ?>" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone3.png" />
							<h2>Avaliacão: <strong>Reserva Ovariana</strong></h2>
							<p>Exames para avaliação da qualidade e quantidade dos óvulos que podem propiciar a gravidez bem sucedida </p>
							<button>Saiba mais</button>
						</a>

						<a href="#" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone4.png" />
							<h2>Obstetrícia</h2>
							<p>Todo o acompanhamento durante a gravidez, no momento do parto e na fase puerpéria</p>
						</a>
						
						<a href="#" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone5.png" />
							<h2>Pré-natal</h2>
							<p>O desenvolvimento do feto, acompanhado de pertinho por consultas períodicas</p>
						</a>
						
						<a href="#" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone6.png" />
							<h2>Ultrassonografia</h2>
							<p>Um dos exames mais importantes da gestação, para verificação da saúde da mamãe e de seu bebê</p>
						</a>
						
						<a href="#" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone7.png" />
							<h2>Ginecologia</h2>
							<p>O acompanhamento periódico da saúde da mulher, desde da idade mais jovem à sua fase mais madura</p>
						</a>
						
						<a href="<?php echo home_url('endometriose'); ?>" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone8.png" />
							<h2>Endometriose</h2>
							<p>A identificação e o tratamento das diferentes manifestações da doença, que além de afetar o bem-estar das mulheres, pode comprometer a fertilidade feminina</p>
							<button>Saiba mais</button>
						</a>
						
						<a href="#" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone9.png" />
							<h2>Fisioterapia <strong>pélvica</strong></h2>
							<p>Todo o trabalho fisioterápico na região pélvica para assegurar melhores condicões físicas da mamãe para o momento do parto</p>
						</a>

						<a href="#" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone10.png" />
							<h2>Nutrologia</h2>
							<p>A elaboração da dieta mais adequada para cada gestante, com seu acompanhamento nutricional personalizado</p>
						</a>
						
						<a href="#" class="item col-12 col-sm-6 col-md-4 align-self-start">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tratamentos/icone11.png" />
							<h2>Tratamentos <strong>íntimos funcionais</strong></h2>
							<p>O tratamento que visa a melhoria do bem-estar íntimo e da estima das pacientes</p>
						</a>

					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endwhile; ?>

<?php get_footer();