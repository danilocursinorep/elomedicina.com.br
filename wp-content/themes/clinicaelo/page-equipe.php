<?php get_header(); ?>


	<section class="atalhoequipe">
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-8 col-lg-8 align-self-center">
					<ul class="medicas">
						<li>Médicas</li>
						<li><a href="#fernanda">Dra. Fernanda Valente</a></li>
						<li><a href="#rosane">Dra. Rosane Rodrigues</a></li>
						<li><a href="#carla">Dra. Carla Iaconelli</a></li>
					</ul>
					<?php if (have_rows('equipe')): ?>
					<ul class="especialidades">
					<?php while (have_rows('equipe')):the_row(); ?>
						<li><a href="#<?php echo strtolower(removeACC(get_sub_field('cargo'))); ?>"><?php the_sub_field('cargo'); ?></a></li>
					<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>

	<section id="fernanda" class="dras">
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-10 col-lg-9 align-self-center">
					<div class="row">
						<div class="col-12 col-lg-7 align-self-center">
							<img class="linha" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
							<h2><strong>Dra.</strong> Fernanda Valente</h2>
							<img class="avatar mobile" src="<?php echo get_template_directory_uri(); ?>/assets/img/fernanda.png">
							<span>Especialista em Reprodução Assistida<br/> e Endoscopia Ginecológica</span>
							<span class="crm">CRM 118.913  •  RQE 30.437</span>
							<ul class="redes mobile">
								<li>
									<a target="_blank" href="mailto:fernanda@elomedicina.com.br">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png">
										<span>fernanda@elomedicina.com.br</span>
									</a>
								</li>
								<!-- <li>
									<a target="_blank" href="#">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/lattes.png" class="lattes" />
									</a>
								</li> -->
							</ul>
							<div class="modal fade" id="modalFernanda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-body">
											<span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
											<h2>Dra. Fernanda Valente</h2>
											<div class="item">
												<ul class="lattes">
													<li>Pós-graduação lato sensu em Reprodução Humana Assistida pela Associação Instituto Sapientiae, em São Paulo;</li>
													<li>Especialização em Endoscopia Ginecológica (laparoscopia e histeroscopia) no Hospital Pérola Byington, em São Paulo;</li>
													<li>Curso de ultrassonografia em ginecologia e obstetrícia pelo Cetrus, em São Paulo;</li>
													<li>Médica e sócia da Invita Medicina Reprodutiva desde 2012;</li>
													<li>Médica do Fertility Medical Group, em São Paulo, desde 2007;</li>
													<li>Título de especialista em Ginecologia e Obstetrícia pela Febrasgo (TEGO);</li>
													<li>Título de especialista em Endoscopia Ginecológica pela Febrasgo;</li>
													<li>Afiliada à Associação de Ginecologia e Obstetrícia do Estado de São Paulo (SOGESP), à Sociedade Brasileira de Reprodução Assistida (SBRA) e à Associação Brasileira de Endometriose e Ginecologia Minimamente Invasiva (SBE);</li>
													<li>Médica colaboradora da pós-graduação em Reprodução Humana Assistida da Associação Instituto Sapientiae em aulas teóricas e práticas.</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<p>A Dra. Fernanda já participou do tratamento de mais de 2.000 pacientes, realizando estimulação ovariana, ultrassonografias seriadas para controle da ovulação, punção de ovário para captação de óvulos e transferências embrionárias, além de auxiliar em procedimentos cirúrgicos para captação de espermatozoides. Há 10 anos, realiza cirurgias ginecológicas minimamente invasivas através de laparoscopia e histeroscopia. Assim, aliando experiência e um tratamento humanizado, é possível proporcionar qualidade e confiança na hora de engravidar.</p>
							<ul class="lattes">
								<li>Graduada em Medicina pela Universidade Federal do Amazonas (UFAM);</li>
								<li>Residência Médica em Ginecologia e Obstetrícia pela Faculdade de Medicina de Jundiaí (FMJ), em São Paulo;</li>
								<li>Especialização em Reprodução Humana Assistida no Hospital Pérola Byington, em São Paulo;</li>
							</ul>
							<button data-toggle="modal" data-target="#modalFernanda">Continue</button>
						</div>
						<div class="col-12 col-lg-5 align-self-center desktop">
							<img class="avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/fernanda.png">
							<div class="row justify-content-center">
								<div class="col-12 col-lg-8">
									<ul class="redes">
										<li>
											<a target="_blank" href="mailto:fernanda@elomedicina.com.br">
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png">
												<span>fernanda@elomedicina.com.br</span>
											</a>
										</li>
										<!-- <li>
											<a target="_blank" href="#">
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/lattes.png" class="lattes" />
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="rosane" class="dras">
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-10 col-lg-9 align-self-center">
					<div class="row">
						<div class="col-12 col-lg-5 align-self-center desktop">
							<img class="avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/rosane.png">
							<div class="row justify-content-center">
								<div class="col-12 col-lg-8">
									<ul class="redes">
										<li>
											<a target="_blank" href="mailto:rosane@elomedicina.com.br">
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png">
												<span>rosane@elomedicina.com.br</span>
											</a>
										</li>
										<!-- <li>
											<a target="_blank" href="#">
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/lattes.png" class="lattes" />
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
						<div class="col-12 col-lg-7 align-self-center">
							<img class="linha" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
							<h2><strong>Dra.</strong> Rosane Rodrigues</h2>
							<img class="avatar mobile" src="<?php echo get_template_directory_uri(); ?>/assets/img/rosane.png">
							<span>Especialista em Reprodução Assistida,<br> Endometriose e Ginecologia</span>
							<span class="crm">CRM 110.213   •  RQE 866881</span>
							<ul class="redes mobile">
								<li>
									<a target="_blank" href="mailto:rosane@elomedicina.com.br">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png">
										<span>rosane@elomedicina.com.br</span>
									</a>
								</li>
								<!-- <li>
									<a target="_blank" href="#">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/lattes.png" class="lattes" />
									</a>
								</li> -->
							</ul>
							<div class="modal fade" id="modalRosane" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-body">
											<span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
											<h2>Dra. Rosane Rodrigues</h2>
											<div class="item">
												<ul class="lattes">
													<li>Especialização em Reprodução Humana Assistida pela Faculdade de Ciências Médicas da Santa Casa de São Paulo, São Paulo, SP (2005);</li>
													<li>Especialização em Endoscopia Ginecológica (Laparoscopia e Histeroscopia) na Faculdade de Medicina do ABC (2006 a 2007);</li>
													<li>Mestre em tocoginecologia pela Faculdade de Ciências Medicas da Santa Casa de São Paulo, São Paulo, SP (2011);</li>
													<li>Pós graduação em Endoscopia Ginecológica pelo Instituto Crispi de Cirurgias Minimamente Invasivas (2013 a 2017);</li>
													<li>Pós graduação em Laparoscopia Cirurgião Pélvico Pleno pelo Instituto Crispi de Cirurgias Minimamente Invasivas (2018 - em andamento);</li>
													<li>Curso de Ultrassonografia ginecológica e obstétrica pela CETRUS, SP;</li>
													<li>Professora da Pós Graduação em Reprodução Humana Assistida do Instituto Sapientiae, SP;</li>
													<li>Médica parceira do Fertility Medical Group, SP, desde 2007;</li>
													<li>Médica com especialização em Medicina Reprodutiva da Invita SP;</li>
													<li>Médica com especialização em Endometriose do Instituto Crispi SP.</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<p>A Dra. Rosane Rodrigues é especialista em Medicina Reprodutiva e Endometriose, atuando há mais de 15 anos nessas áreas. Formada pela Faculdade de Medicina na Universidade Federal de Sergipe e fez residência médica em ginecologia e obstetrícia no Hospital Ipiranga em São Paulo - SP.</p>
							<ul class="lattes">
								<li>Graduação pela Faculdade de Medicina da Universidade Federal de Sergipe (UFS) (2002);</li>
									<li>Residência Médica em Ginecologia e Obstetrícia no Hospital Ipiranga,<?php echo strtolower(removeACC(get_sub_field('cargo'))); ?> São Paulo, SP (2003 a 2004);</li>
							</ul>
							<button data-toggle="modal" data-target="#modalRosane">Continue</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php if (!have_rows('equipe')): ?>
	<section id="carla" class="dras marginbottom">
		<div class="grad"></div>
	<?php else: ?>
	<section id="carla" class="dras">
	<?php endif; ?>
		<div class="container">
			<div class="row itens justify-content-center">
				<div class="col-10 col-lg-9 align-self-center">
					<div class="row">
						<div class="col-12 col-lg-7 align-self-center">
							<img class="linha" src="<?php echo get_template_directory_uri(); ?>/assets/img/linhaH.png">
							<h2><strong>Dra.</strong> Carla Iaconelli</h2>
							<img class="avatar mobile" src="<?php echo get_template_directory_uri(); ?>/assets/img/carla.png">
							<span>Especialista em Reprodução Assistida,<br> Ginecologia e Obstetrícia</span>
							<span class="crm">CRM 124.292  •  RQE 51.682</span>
							<ul class="redes mobile">
								<li>
									<a target="_blank" href="mailto:carlaiaconelli@elomedicina.com.br">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png">
										<span>carlaiaconelli@elomedicina.com.br</span>
									</a>
								</li>
								<!-- <li>
									<a target="_blank" href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4411696T6">
										<img src="<?php echo get_template_directory_uri(); ?>/assets/img/lattes.png" class="lattes" />
									</a>
								</li> -->
							</ul>
							<div class="modal fade" id="modalCarla" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-body">
											<span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
											<h2>Dra. Carla Iaconelli</h2>
											<div class="item">
												<h3>Formação Acadêmica/Titulação</h3>

												<p><strong>2016</strong>
												Professora do curso Reprodução Humana Assistida
												Professora do curso de Pós Graduação lato sensu em Reprodução Humana Assistida na Associação Instituto Sapientiae - 2016.</p>

												<p><strong>2014</strong>
												Mestrado em Medicina
												Em 2014 conquistou o título de Mestre em Medicina pela Faculdade de Ciências Médicas da Santa Casa de São Paulo.</p>


												<p><strong>2012</strong>
												Especialização em ginecologia minimamente invasiva
												Hospital Sírio-Libanês, SIRIO-LIBANÊS, Brasil. Título: Histeroscopia Prévia a Ciclos de Fertilização in Vitro. Orientador: Dr. João Antonio Dias Júnior.</p>

												<p><strong>2009</strong>
												Especialização em Reprodução Humana Assistida
												Associação Instituto Sapientiae, SAPIENTIAE, Brasil. Título: Prevenção e Fatores Preditivos da Síndrome de Hiperestímulo Ovariano. Orientador: Wagner de Camargo Penteado Busato.</p>

												<p><strong>2009</strong>
												Fellow no IVI
												Fellow no Instituto Valenciano de Infertilidade (IVI) Barcelona, Espanha.</p>

												<p><strong>2009</strong>
												Especialização em Ultrassonografia em Medicina Interna
												Centro de Treinamento em Ultrassonografia de São Paulo.</p>

												<p><strong>2009</strong>
												Especialização em Ultrassonografia em Ginecologia e Obstetrícia
												Centro de Treinamento em Ultrassonografia de São Paulo.</p>

												<p><strong>2008</strong>
												Especialização em Ultrassonografia Transvaginal
												Centro de Treinamento em Ultrassonografia de São Paulo.</p>

												<p><strong>2007</strong>
												Especialização - Residência médica
												Especialização em Clínica Médica pelo Hospital do Servidor Público Estadual (IANSPE)</p>
											</div>
											<div class="item">
												<h3>Prêmios e Títulos</h3>

												<p><strong>2013</strong>
												Título de Especialista em Ginecologia e Obstetrícia
												Título de Especialista em Ginecologia e Obstetrícia TEGO 2013, Federação Brasileira das Associações de Ginecologia e Obstetrícia (FEBRASGO).</p>

												<p><strong>2012</strong>
												Título De Capacitação em Reprodução Assistida
												Título De Capacitação em Reprodução Assistida, Sociedade Brasileira de Reprodução Assistida.</p>

												<p><strong>2006</strong>
												Membro Efetivo da Liga de Hipertensão Arterial Sistêmica
												Membro Efetivo da Liga de Hipertensão Arterial Sistêmica, Faculdade de Medicina da Universidade de Santo Amaro.</p>

												<p><strong>2005</strong>
												Diretora Científica da Liga de Câncer da Pele
												Diretora Científica da Liga de Câncer da Pele, Faculdade de Medicina da Universidade de Santo Amaro.</p>

												<p><strong>2004</strong>
												Prêmio de Melhor Acadêmica Expositora
												Prêmio de Melhor Acadêmica Expositora, XXV Congresso Acadêmico Médico.</p>


												<p><strong>2003</strong>
												Membro Efetivo da Liga de Saúde da Mulher
												Membro Efetivo da Liga de Saúde da Mulher, Faculdade de Medicina da Universidade de Santo Amaro.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<p>Atua na assistência a casais e pessoas que desejam formar uma família e na promoção de saúde a mulheres em todas as fases da vida. Há 13 anos na área de reprodução humana assistida como Staff de uma das primeiras e melhores clínicas do Brasil, a Fertility Medical Group, teve aprendizado internacional pelo Instituto Valenciano de Infertilidad (IVI), na Espanha. Tem experiência com as técnicas em medicina reprodutiva, realiza cirurgias ginecológicas minimamente invasivas, obstetrícia, atendimentos a pacientes na menopausa e climatério, aconselhamento e tratamento para criopreservação de óvulos, oncofertilidade, planejamento familiar e contracepção.</p>
							<p>Acredita na humanização da medicina, na relação médico-paciente de confiança e no papel central da ciência na evolução da sociedade.</p>
							<ul class="lattes">
								<li>Em 2014 conquistou o título de Mestre em Medicina pela Faculdade de Ciências Médicas da Santa Casa de São Paulo.</li>
								<li>Atua na área de Reprodução Humana Assistida como corpo clínico na Clínica Fertility e em consultório particular.</li>
							</ul>
							<button data-toggle="modal" data-target="#modalCarla">Continue</button>
						</div>
						<div class="col-12 col-lg-5 align-self-center desktop">
							<img class="avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/carla.png">
							<div class="row justify-content-center">
								<div class="col-12 col-lg-9">
									<ul class="redes">
										<li>
											<a target="_blank" href="mailto:carlaiaconelli@elomedicina.com.br">
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png">
												<span>carlaiaconelli@elomedicina.com.br</span>
											</a>
										</li>
										<!-- <li>
											<a target="_blank" href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4411696T6">
												<img src="<?php echo get_template_directory_uri(); ?>/assets/img/lattes.png" class="lattes" />
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php if (have_rows('equipe')): ?>
	<div class="pessoas">
	<div class="grad"></div>
		<?php while (have_rows('equipe')):the_row(); ?>
			<div id="<?php echo strtolower(removeACC(get_sub_field('cargo'))); ?>">

				<?php $cargo = get_sub_field('cargo'); ?>
				
				<?php if (have_rows('pessoas')): ?>
					<?php while (have_rows('pessoas')):the_row(); ?>
						<section class="dras mini">
							<div class="container">
								<div class="row itens justify-content-center">
									<div class="col-10 col-lg-9 align-self-center">
										<div class="row">
											<div class="col-10 col-lg-5 align-self-center desktop">
												<div class="img" style="background-image: url(<?php the_sub_field('avatar'); ?>);">
												</div>
											</div>
											<div class="col-10 col-lg-7 align-self-center">
												<div class="img mobile" style="background-image: url(<?php the_sub_field('avatar'); ?>);">
												</div>
												<h2><?php the_sub_field('nome') ?></h2>
												<h3><?php echo $cargo ?> | Código <?php the_sub_field('codigo') ?></h3>

												<p><?php the_sub_field('lattes') ?></p>
												<button>Continue</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	</div>
	<?php endif; ?>

	<section class="ondas">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/onda3.png" class="onda" />
	</section>

<?php get_footer();