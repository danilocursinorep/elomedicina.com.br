<?php if (!is_front_page()): ?>
<form method="get" class="margin50" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
     <input type="text" class="field" name="s" id="s" placeholder="Assunto" />
     <input type="submit" class="field" value="Pesquisar" />
</form>
<?php else: ?>
<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
     <input type="text" class="field" name="s" id="s" placeholder="Buscar" />
</form>
<?php endif; ?>