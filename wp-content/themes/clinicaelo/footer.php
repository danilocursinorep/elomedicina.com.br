<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Clínica_ELO
 */

?>	
	</main><!-- #main -->
	<footer id="colophon" class="site-footer">
		<?php if(!is_page('agendamento')): ?>
		<a href="<?php echo home_url('agendamento'); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/agendaLateral.png" class="agenda lateral" />
		</a>
		<?php endif; ?>
		<a href="https://wa.me/5511940076113" target="_blank">
			<div class="whatsapp lateral"></div>
		</a>
		<div class="redes lateral">
			<a href="https://wa.me/5511940076113" target="_blank" class="item whats"></a>
			<a href="tel:+551138059754" target="_blank" class="item fone"></a>
			<a href="https://www.instagram.com/elo.medicina/" target="_blank" class="item insta"></a>
			<a href="https://www.facebook.com/elo.medicina" target="_blank" class="item face"></a>
			<a href="https://www.youtube.com/channel/UCH4_iQOnJHBJFbF1lYLk5Ng" target="_blank" class="item yout"></a>
		</div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="logo col-10 col-lg-2">
					<a href="<?php echo home_url(); ?>" target="_self">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logoFooter.png" class="logo" />
					</a>
				</div>
				<div class="redes col-10 col-lg-3">
					<ul>
						<li>
							<a href="tel:+551138059754" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeTel.png">
								<span>(11) 3805-9754</span>
							</a>
						</li>
						<li>
							<a href="https://wa.me/5511940076113" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeWhats.png">
								<span>(11) 94007-6113</span>
							</a>
						</li>
						<li>
							<a href="mailto:contato@elomedicina.com.br" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeEmail.png">
								<span>contato@elomedicina.com.br</span>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/elo.medicina/" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeInsta.png">
								<span>/elo.medicina</span>
							</a>
						</li>
						<li>
							<a href="https://www.facebook.com/elo.medicina" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeFacebook.png">
								<span>/elo.medicina</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="redes col-10 col-lg-3">
					<ul>
						<li>
							<a href="<?php echo home_url('agendamento'); ?>" target="_self">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeAgendamento.png">
								<span>Pré-agende sua consulta aqui.</span>
							</a>
						</li>
						<li>
							<a href="https://goo.gl/maps/MLoXuvypLbkwTuPC8" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeMaps.png">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/iconeWaze.png"><br>
								<span class="margin-top">Alameda dos Maracatins Nº 426 • Conjunto 804 Moema, São Paulo/SP CEP 04089-010</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="sobre col-10">
					<p><?php echo date('Y'); ?> &copy; Todos os direitos reservados. O conteúdo deste site foi elaborado pela equipe da Clínica Elo Medicina Reprodutiva e as informações aqui contidas têm caráter meramente informativo e educacional. Não deve ser utilizado para realizar autodiagnóstico ou automedicação. Em caso de dúvidas, consulte seu médico. Somente ele está habilitado a praticar o ato médico, conforme recomendação do Conselho Federal de Medicina. Todas as imagens contidas no site são meramente ilustrativas e foram compradas em banco de imagens, portanto não utilizam imagens de pacientes. Diretora Clínica Responsável: Rosane Santana Rodrigues - CRM 110.213.</p>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<script type="text/javascript">
	/** This section is only needed once per page if manually copying **/
	if (typeof MauticSDKLoaded == 'undefined') {
		var MauticFormCallback = {};
		var MauticSDKLoaded = true;
		var head            = document.getElementsByTagName('head')[0];
		var script          = document.createElement('script');
		script.type         = 'text/javascript';
		script.src          = 'https://mkt.elomedicina.com.br/media/js/mautic-form.js';
		script.onload       = function() {
		MauticSDK.onLoad();
		};
		head.appendChild(script);
		var MauticDomain = 'https://mkt.elomedicina.com.br';
		var MauticLang   = {
			'submittingMessage': "Por favor, aguarde..."
		}
	} else if (typeof MauticSDK != 'undefined') {
		MauticSDK.onLoad();
	}
     MauticFormCallback['preagendamentosite'] = {
          onResponse: function(response) {
               $('.modalagendamento').modal('show');
          },
     };
     MauticFormCallback['preagendamento2site'] = {
          onResponse: function(response) {
               $('.modalagendamento').modal('show');
          },
     };
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"></script>
<!-- <script type="text/javascript">

	var can = elm.children('canvas').attr('id');
	var val = elm.children('.valor').text();

	var graph = new Chart(document.getElementById(can).getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [val, (100 - val)],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa de Fertilização hormonal',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			layout: {
				padding: {
					left: 0,
					right: 4,
					top: 0,
					bottom: 0
				}
			},		
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var CliclosTrans = new Chart(document.getElementById('CliclosTrans').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [72, 28],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Ciclos transferidos em Dia 5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaGest = new Chart(document.getElementById('TaxaGest').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [40, 60],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa de gestação / Ciclo transferido',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaGestCiclo = new Chart(document.getElementById('TaxaGestCiclo').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [44.4, 63.6],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaImp = new Chart(document.getElementById('TaxaImp').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [25.4, 74.6],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaImpD5 = new Chart(document.getElementById('TaxaImpD5').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [26.7, 73.3],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaAbo = new Chart(document.getElementById('TaxaAbo').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [12.5, 87.5],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaAboD5 = new Chart(document.getElementById('TaxaAboD5').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [16.7, 83.3],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var CliclosTransEx = new Chart(document.getElementById('CliclosTransEx').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [24, 76],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var CiclosNao = new Chart(document.getElementById('CiclosNao').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [51.2, 48.8],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaGestTrans = new Chart(document.getElementById('TaxaGestTrans').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [63.3, 36.7],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaImpTrans = new Chart(document.getElementById('TaxaImpTrans').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [50.6, 49.4],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var TaxaAboBlue = new Chart(document.getElementById('TaxaAboBlue').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [10.3, 89.7],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
	var GestGer = new Chart(document.getElementById('GestGer').getContext('2d'), {
		type: 'pie',
		data: {
			datasets: [{
				data: [54.4, 44.6],
				backgroundColor: [
					'#671B51',
					'#BEA07B'
				]
			}],
			labels: [
				'Taxa gestação /Ciclo transferido em D5',
				'Yellow'
			]
		},
		options: {
			legend: {
				display: false
			},
			responsive: false,
			maintainAspectRatio: true
		}
	});
</script> -->
<?php wp_footer(); ?>
</body>
</html>
