<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

define('WP_AUTO_UPDATE_CORE', 'minor');

/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'clinicaelo' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'root' );

/** Nome do host do MySQL */
define( 'DB_HOST', '127.0.0.1' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aL=RiM&,ZFo18ez^IL==-HL&)A.}{m>)oD2hwhNrs!`QkL;F}<FkKaCsbSu],4~o' );
define( 'SECURE_AUTH_KEY',  'Y|>v~1R2tW+!.Fm=Y;lQ0,F*[J2`:nB/rWO/YE:O|D*1;Vq+@{@ +b.O2#Bp,uT:' );
define( 'LOGGED_IN_KEY',    '(#A0W4g$^EWe@w$:P4Y#43E,-ec8NO>SdW{5M-5,e8p]RqkSk@KN0]}DKn4;;CEw' );
define( 'NONCE_KEY',        '>U<HQ(!pmT}nDlC*,?aP,D#M$smi1|iZ>rc#g;Cl1;N LOiD/s!i0&{i%=AE29zR' );
define( 'AUTH_SALT',        '0hnD*sbDblcs#(I8]Q,PE+t66g17D=(h:+78Ov[*01Yd:~:Mol0Ku>~8A`;nkjM5' );
define( 'SECURE_AUTH_SALT', '~o`/MQ+lH[g0!>WwlFoni`I]%b/@>8Yb4Uuw<V:d4HO;Zra%l18xTEM-b@qi[#su' );
define( 'LOGGED_IN_SALT',   ',>~-%mMas)[BfI2T^H[`wq.OY1A[%W`u_jadj$cp ]WvN=V)MsN/{50Z(Er+7BBR' );
define( 'NONCE_SALT',       '2Q@=gdb21B$aHPCpj)#&.LqY *cWx,.; n>q3ROkhC@&fP[=@=jsT5oE3 wBP<jl' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'clielo_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', true);
define('FS_METHOD', 'direct');

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
     define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
